plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.31")
    id("com.vanniktech.maven.publish") version "0.8.0"
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("tech.codingzen:either:1.0.1")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}
